/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mts.gateway.web.rest.vm;
