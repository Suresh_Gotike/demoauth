import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MtsauthSharedModule } from 'app/shared/shared.module';
import { MtsauthCoreModule } from 'app/core/core.module';
import { MtsauthAppRoutingModule } from './app-routing.module';
import { MtsauthHomeModule } from './home/home.module';
import { MtsauthEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    MtsauthSharedModule,
    MtsauthCoreModule,
    MtsauthHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MtsauthEntityModule,
    MtsauthAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class MtsauthAppModule {}
