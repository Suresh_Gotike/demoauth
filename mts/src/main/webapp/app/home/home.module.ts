import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MtsauthSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [MtsauthSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class MtsauthHomeModule {}
